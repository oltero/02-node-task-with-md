const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

/**
 * This function multiplies two numbers
 * @param {Number} first first number
 * @param {Number} second second number
 * @returns {Number} Multiplication of the two params
 */
const multip = (first, second) => {
  return first*second;
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

